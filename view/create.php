<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../resource/css/bootstrap.min.css">
    <script src="../resource/js/jquery.min.js"></script>
    <script src="../resource/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <a href="index.php" class="btn btn-info">DashBoard</a>
    <h2>Insert Student</h2>
    <table class="table table-bordered">
        <form role="form"method="post" action="store.php">
            <!--<div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" placeholder="Enter email">
            </div>-->
            <div class="form-group">
                <label>First Name</label>
                <input type="text" id="text" name="firstname" class="form-control">
            </div>
            <div class="form-group">
                <label>Middle Name</label>
                <input type="text" id="text" name="middlename" class="form-control">
            </div>
            <div class="form-group">
                <label>Last Name</label>
                <input type="text" id="text" name="lastname" class="form-control">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </table>

</div>

</body>
</html>
