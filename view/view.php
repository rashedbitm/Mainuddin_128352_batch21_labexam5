<?php
include_once ('../vendor/autoload.php');
use App\Student\Student;

$student = new Student();
$getAllData= $student->prepareData($_GET)->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Student View</title>
    <link rel="stylesheet" href="../resource/css/bootstrap.min.css" type="text/css">
    <!--<script href="../resource/js/bootstrap.js"/>-->
</head>
<body>
<div class="container">
    <h1>Student Details</h1>
    <a href="index.php" class="btn btn-info">DashBoard</a>
    <table class="table table-bordered">
        <tr>

            <td>Id</td>
            <td>First Name</td>
            <td>Last Name</td>
            <td>Middle Name</td>

        </tr>

        <tr>

            <td><?php echo $getAllData->id; ?></td>
            <td><?php echo $getAllData->firstname; ?></td>
            <td><?php echo $getAllData->lastname; ?></td>
            <td><?php echo $getAllData->middlename; ?></td>

        </tr>
    </table>

</div>
</body>
</html>