<?php

include_once('../vendor/autoload.php');

use App\Student\Student;


$student = new Student();
$student->prepareData($_POST);
$student->create();