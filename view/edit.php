<?php
include_once ('../vendor/autoload.php');
use App\Student\Student;

$student = new Student();
$getAllData= $student->prepareData($_GET)->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../resource/css/bootstrap.min.css">
    <script src="../resource/js/jquery.min.js"></script>
    <script src="../resource/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Update Student</h2>
    <a href="index.php" class="btn btn-info">DashBoard</a> <a href="create.php" class="btn btn-info">Insert More</a>
    <table class="table table-bordered">
        <form role="form"method="post" action="store.php">
            <!--<div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" placeholder="Enter email">
            </div>-->
            <div class="form-group">
                <input type="hidden"  name="id" value="<?php echo $getAllData->firstname; ?>">
                <label>First Name</label>
                <input type="text" id="text" name="firstname" class="form-control" value="<?php echo $getAllData->firstname; ?>">
            </div>
            <div class="form-group">
                <label>Middle Name</label>
                <input type="text" id="text" name="middlename" class="form-control" value="<?php echo $getAllData->middlename; ?>">
            </div>
            <div class="form-group">
                <label>Last Name</label>
                <input type="text" id="text" name="lastname" class="form-control" value="<?php echo $getAllData->lastname; ?>">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </table>

</div>

</body>
</html>
