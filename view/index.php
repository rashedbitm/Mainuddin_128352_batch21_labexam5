<?php
include_once ('../vendor/autoload.php');
use App\Student\Student;

$student = new Student();
$getAllData = $student->prepareData($_GET)->index();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Student DashBoard</title>
    <link rel="stylesheet" href="../resource/css/bootstrap.min.css" type="text/css">
    <!--<script href="../resource/js/bootstrap.js"/>-->
</head>
<body>
<div class="container">
    <h1>Student List</h1>
    <a href="create.php" class="btn btn-info">Insert Again</a>
    <div class="table">
        <table class="table table-bordered">
            <tr>
                <td>SL</td>
                <td>Id</td>
                <td>First Name</td>
                <td>Middle Name</td>
                <td>Last Name</td>
                <td>Action</td>
            </tr>

            <tr>
                <?php
                $sl=0;
                foreach($getAllData as $student) {
                $sl++;
                ?>
                <td><?php echo $sl; ?></td>
                <td><?php echo $student->id; ?></td>
                <td><?php echo $student->firstname; ?></td>
                <td><?php echo $student->middlename; ?></td>
                <td><?php echo $student->lastname; ?></td>
                <td>
                    <a href="view.php?id=<?php echo $student->id; ?>" class="btn btn-info">View</a>
                    <a href="edit.php?id=<?php echo $student->id; ?>" class="btn btn-info">Edit</a>
                    <a href="delete.php?id=<?php echo $student->id; ?>" class="btn btn-info" onclick="return confirm('Do you really want to delete this Student?');">Delete</a>
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>

</div>
</body>
</html>
